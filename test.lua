project "test"
	kind "ConsoleApp"
	language "C"
	files "src/test/*.c"
	includedirs { "src/"}
	links { "mosaic" }
	filter "configurations:debug"
		buildoptions { "-fno-omit-frame-pointer" }
		buildoptions { "-fsanitize=undefined,address" }
		linkoptions { "-fsanitize=undefined,address" }
